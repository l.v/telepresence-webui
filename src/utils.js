import moment from "moment";

export const pad = (val, nb, padWith = ' ') => String(padWith).repeat(nb - String(val).length) + val;
export const formatHours = (date) => moment(date).format("HH:mm");