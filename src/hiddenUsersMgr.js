import store from "./store/store";
import { setHiddenUsers } from "./store/actions";

//this whole thing feels weird - it's tied to the HiddenUsers component
//should I just store hiddenUsers in an exported variable here
//and import it where I need it?
//ohwell, don't fix what ain't broke

readFromLocation();


function readFromLocation() {
    const query = new URLSearchParams(window.location.search);
    if (query.has("hide")) {
        const users = readSerializedUsers(query.get("hide"));

        store.dispatch(setHiddenUsers(users));
    }
}


export function serializeHiddenUsers(userIds) {
    return [...userIds]
        .join(",")
}

export function readSerializedUsers(userIdsStr) {
    return userIdsStr.split(",")
        .filter(id => id.length > 0)
}
