import { useLocation } from 'react-router-dom';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

export function updateQueryParams(query, param) {
    const newLoc = {...history.location};

    const searchParams = new URLSearchParams(newLoc.search);
    searchParams.set(query, param);

    newLoc.search = searchParams.toString();

    history.push(newLoc);
}

export function useQuery() {
    return new URLSearchParams(useLocation().search);
}
