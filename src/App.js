import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import {getInitData} from "./api";
import effectSSE from "./api-ssemanager";
import store from "./store/store";
import {setUsers, setVChans, setTChans} from "./store/actions";

import EmarjPanel from "./components/Emarj/Emarj";
import VoiceChannelList from "./components/VCViewer/VoiceChannelList";
import { HiddenUsers } from './components/HiddenUsers';

function App() {
  const {serverId} = useParams();
  const [botStatus, setBotStatus] = useState({loading: true, botInGuild: false, serverAvailable: false});
  const [showHiddenUsers, setShow] = useState(false);

  useEffect(() => effectSSE(serverId, store.dispatch), [serverId]);

  useEffect(() => {
    getInitData(new Date().toISOString(), serverId)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setBotStatus({loading: false, botInGuild: data.botIsInGuild, serverAvailable: data.serverAvailable});

        if (data.users)
          store.dispatch(setUsers(data.users));
        if (data.vchans)
          store.dispatch(setVChans(data.vchans));
        if (data.tchans)
          store.dispatch(setTChans(data.tchans));
      });
  }, [serverId]);

  return (
    <>
      <Container fluid>
        <Row noGutters>
          <Col>
            <EmarjPanel serverId={serverId} />
          </Col>
          <Col sm="auto">
            <VoiceChannelList serverId={serverId} />
          </Col>
        </Row>
      </Container>
      <Alert variant="dark" className="mx-2">
        serverId: {serverId}
        <span className={`mx-2 ${!botStatus.loading && botStatus.botInGuild ? "text-success" : "text-danger"}`}>
          Bot is in server: {botStatus.loading
            ? <Spinner size="sm" animation="border" role="status" />
            : botStatus.botInGuild ? "yes" : "no"
          }
        </span>
        <span className={`mx-2 ${!botStatus.loading && botStatus.serverAvailable ? "text-success" : "text-danger"}`}>
          Server available: {botStatus.loading
            ? <Spinner size="sm" animation="border" role="status" />
            : botStatus.serverAvailable ? "yes" : "no"
          }
        </span>
        <button onClick={() => setShow(true)}>Hidden users</button>
      </Alert>
      <HiddenUsers show={showHiddenUsers} hide={() => setShow(false)}/>
    </>
  );
}

export default App;