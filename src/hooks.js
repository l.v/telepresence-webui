import { useSelector } from "react-redux";


export function useGetHiddenUsersSet() {
  const hiddenUserIds = useSelector(state => state.hiddenUsers);
  return new Set(hiddenUserIds);
}

export function useGetVisibleUsers() {
  const hiddenUsers = useGetHiddenUsersSet();

  return useSelector(state => state.users)
    .filter(user => !hiddenUsers.has(user.id))
}

export function useGetMessages() {
  return useSelector(state => state.msgs)
    .map(msg => ({
      ...msg,
      date: new Date(msg.date)
    }))
}

export function useGetTodaysMessagesForChannel(date, channelId) {
    const dateEnd = date.getTime() + 24 * 60 * 60 * 1000;

    return useGetMessages()
      .filter(msg => msg && msg.date > date && msg.date < dateEnd && msg.chan === channelId)
}

export function useGetTextChannelsForServer(serverId) {
  return useSelector(state => state.tChans)
    .filter(chan => chan.serverId === serverId)
    .sort((a, b) => a.pos - b.pos)
}
