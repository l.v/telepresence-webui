import React from 'react';

import "./EmarjTable.css";
import Table from "react-bootstrap/Table";
import UserRow from "./UserRow";

import { useGetTodaysMessagesForChannel, useGetVisibleUsers } from "../../hooks";
import { EditableClockingDateHeader } from './EditableClockingDateHeader';


export default function({clockingDates, date, setClockingDate, discordChannel, discordMode}) {
  const msgs = useGetTodaysMessagesForChannel(date, discordChannel);
  const users = useGetVisibleUsers();

  return <Table bordered size="sm">
    <thead className="centered">
      <tr>
        <td>name</td>
        {
          clockingDates.map((clockingDate, index) =>
            <td key={index}>
              <EditableClockingDateHeader
                clockingDate={clockingDate}
                index={index}
                setClockingDate={(date) => setClockingDate(index, date)}
              />
            </td>
          )
        }
      </tr>
    </thead>
    <tbody>
      {
        users
        .sort((userA, userB) => userA.name.localeCompare(userB.name[0]))
        .map((user, index) =>
          <UserRow
            key={index}
            user={user}
            userMsgs={msgs.filter(msg => {
              return msg.author === user.id;
            })}
            clockingDates={clockingDates}
            date={date}
            discordMode={discordMode}
          />
        )
      }
    </tbody>
  </Table>
}
