import React from 'react';

const dayTable = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];

const formatDate = (date = new Date()) => `${dayTable[date.getDay()]} ${date.getDate()} ${date.getMonth() + 1} ${date.getFullYear()}`


export default function DatePicker({date, dispatchDate}) {
  return <div>
      <button onClick={() => dispatchDate({type: 'prev'})}>{'<'}</button>
      <span>{formatDate(date)}</span>
      <button onClick={() => dispatchDate({type: 'next'})}>{'>'}</button>
      <button onClick={() => dispatchDate({type: 'today'})}>{'go to today'}</button>
    </div>
}