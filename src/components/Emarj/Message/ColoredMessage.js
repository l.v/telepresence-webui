import React from 'react';
import './ColoredMessage.css';
import { formatHours } from "../../../utils";

export default function ColoredMessage({user, msg, clockingDate}) {
  const timeDistance = clockingDate > msg.date ? clockingDate - msg.date : msg.date - clockingDate;
  const scaled = scaleBetween(timeDistance, 0, 100, 0, 1000 * 60 * 60 * 2);
  const bgColor = getRedGreenGradientFromPercent(scaled);

  return <td className="message-container" style={{backgroundColor: `rgb(${bgColor.r}, ${bgColor.g}, ${bgColor.b})`}}>
    <div className="message-content">{msg.content}</div>
    <div className="message-date">{formatHours(msg.date)}</div>
  </td>
}


function scaleBetween(unscaledNum, minAllowed, maxAllowed, min, max) {
  return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
}

/*
function getRedGreenGradientFromPercent(percent) {
  return {
    r: (255  * percent) / 100,
    g: (255 * (100 - percent)) / 100,
    b: 0
  };
}
*/

function getRedGreenGradientFromPercent(percent) {
  const power = percent / 100;
  if (power <= 0.5 ) {
    return {
      r: (2 * power) * 255,
      g: 255,
      b: 0
    }
  } else {
    return {
      r: 255,
      g: (1 - 2 * (power - 0.5)) * 255,
      b: 0
    }
  }
}
