import React from 'react';
import './DiscordMessage.css';

const formatDiscordLinkToMessage = (serverId, channelId, msgId) => `https://discordapp.com/channels/${serverId}/${channelId}/${msgId}`;

export default function DiscordMessage({user, msg}) {
  return <td className="discord-message-wrapper-wrapper">
    <div className="discord-message-wrapper">
      <div className="discord-message-container">
        <img className="discord-message-avatar" src={user.avatarURL || "http://placehold.it/40x40"} alt="" />
        <h2 className="discord-message-header">
          <span className="discord-message-username">{user.name}</span>
          <span className="discord-message-timestamp">
            <a className="hidden-link" href={formatDiscordLinkToMessage(msg.server, msg.chan, msg.id)}>{msg.date.toLocaleString()}</a>
          </span>
        </h2>
        <div className="discord-message-content">{msg.content}</div>
      </div>
    </div>
  </td>
}
