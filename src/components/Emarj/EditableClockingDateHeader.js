import React from 'react';
import moment from "moment";
import TimePicker from "react-time-picker";

export const EditableClockingDateHeader = ({clockingDate, setClockingDate}) => {
    return (
        <TimePicker
            value={moment(clockingDate).format("HH:mm")}
            onChange={time => setClockingDate(moment(time, "HH:mm"))}
            clearIcon={null}
            maxDetail="minute"
            format="HH:mm"

        />
    )
}
