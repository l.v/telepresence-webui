import React from 'react';
import { useSelector } from 'react-redux';

import VoiceChannel from './VoiceChannel';

import { useGetVisibleUsers } from "../../hooks";


function useGetVChannels(serverId) {
  return useSelector(state => state.vChans)
    .filter(chan => chan.serverId === serverId)
    .sort((a, b) => a.pos - b.pos)
}

export default function VoiceChannelList({serverId}) {
  const vChannels = useGetVChannels(serverId);
  const users = useGetVisibleUsers();

return <div>
    {
      [
        <VoiceChannel
          key={vChannels.length}
          vchan={{name: "Non-connectés", id: null}}
          users={users.filter(user => user.VcState.channelId == null)}
        />,
        ...vChannels.map((vchan, index) => 
          <VoiceChannel
            key={index}
            vchan={vchan}
            users={users.filter(user => user.VcState.channelId === vchan.id)}
          />
        )
      ]
    }
  </div>
}
