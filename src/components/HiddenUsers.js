import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Modal from "react-bootstrap/Modal";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";

import {hideUser, unhideUser} from "../store/actions";
import { useGetHiddenUsersSet } from "../hooks";
import { updateQueryParams } from "../queryParamsMngmt";
import { serializeHiddenUsers } from "../hiddenUsersMgr";
import "../hiddenUsersMgr";

export const HiddenUsers = ({show: isVisible, hide}) => {
    const dispatch = useDispatch();
    const users = useSelector(state => state.users);
    const hiddenUserIds = useGetHiddenUsersSet();

    useEffect(() => { //probably should be handled by the reducers
        updateQueryParams("hide", serializeHiddenUsers(hiddenUserIds));
    }, [hiddenUserIds]);

    return (
        <Modal show={isVisible} onHide={() => hide()}>
            <Modal.Header closeButton>
                <Modal.Title>Hidden users</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <ListGroup variant="flush">
                    {
                        users
                        .sort((userA, userB) => hiddenUserIds.has(userA.id) - hiddenUserIds.has(userB.id))
                        .map((user, index) => {
                            const isHidden = hiddenUserIds.has(user.id);
                            return <ListGroup.Item key={index} variant="secondary">
                                <Button onClick={() => dispatch(isHidden ? unhideUser(user.id) : hideUser(user.id)) }>
                                    {isHidden ? "show" : "hide"}
                                </Button>
                                {user.name}
                            </ListGroup.Item>
                        })
                    }
                </ListGroup>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={hide}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}
