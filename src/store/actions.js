export function addMessage(msg) {
    return {
        type: "ADD_MSG",
        msg
    }
}

export function addMessages(msgs) {
    return {
        type: "ADD_MSGS",
        msgs
    }
}

export function setUser(user) {
    return {
        type: "SET_USER",
        user
    }
}

export function setUsers(users) {
    return {
        type: "SET_USERS",
        users
    }
}

export function setVChans(vChans) {
    return {
        type: "SET_VCHANS",
        vChans
    }
}

export function setTChans(tChans) {
    return {
        type: "SET_TCHANS",
        tChans
    }
}


export function setVcStatus(VcState) {
    return {
        type: "SET_VCSTATUS",
        VcState
    }
}

export function hideUser(userId) {
    return {
        type: "HIDE_USER",
        userId
    }
}

export function unhideUser(userId) {
    return {
        type: "UNHIDE_USER",
        userId
    }
}
export function setHiddenUsers(userIds) {
    return {
        type: "SET_HIDDEN_USERS",
        userIds
    }
}
